package com.example.tdd.tdd;

import com.example.tdd.tdd.entidad.Usuario;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UsuarioTest {



    @Test
    void validarNombre() {
        Usuario usuario = new Usuario();


        // Prueba para un nombre válido
        usuario.setNombre("Saul");
        boolean resultadoValido = usuario.validarNombre();
        assertTrue(resultadoValido);

        // Prueba para un nombre inválido
        usuario.setNombre("");
        boolean resultadoInvalido = usuario.validarNombre();
        assertFalse(resultadoInvalido);
    }

    @Test
    void validarApellidoPaterno() {
        Usuario usuario = new Usuario();

        // Prueba para un apellido paterno válido
        usuario.setApellidoPaterno("Lefiqueo");
        boolean resultadoValido = usuario.validarApellidoPaterno();
        assertTrue(resultadoValido);

        // Prueba para un apellido paterno inválido
        usuario.setApellidoPaterno("");
        boolean resultadoInvalido = usuario.validarApellidoPaterno();
        assertFalse(resultadoInvalido);
    }

    @Test
    void validarApellidoMaterno() {
        Usuario usuario = new Usuario();

        // Prueba para un apellido materno válido
        usuario.setApellidoMaterno("Meza");
        boolean resultadoValido = usuario.validarApellidoMaterno();
        assertTrue(resultadoValido);

        // Prueba para un apellido materno inválido
        usuario.setApellidoMaterno("");
        boolean resultadoInvalido = usuario.validarApellidoMaterno();
        assertFalse(resultadoInvalido);
    }

    @Test
    void validarRut() {
        Usuario usuario = new Usuario();

        // Prueba para un Rut válido
        usuario.setRut("12.345.678-9");
        boolean resultadoValido = usuario.validarRut();
        assertTrue(resultadoValido);

        // Prueba para un Rut inválido
        usuario.setRut("12345678434-9"); // Rut sin puntos y numeros extras
        boolean resultadoInvalido = usuario.validarRut();
        assertFalse(resultadoInvalido);
    }

    @Test
    void validarNumeroTelefonico() {
        Usuario usuario = new Usuario();

        // Prueba para un número telefónico válido
        usuario.setNumeroTelefonico("+56923456789");
        boolean resultadoValido = usuario.validarNumeroTelefonico();
        assertTrue(resultadoValido);

        // Prueba para un número telefónico inválido
        usuario.setNumeroTelefonico(""); // Número vacío
        boolean resultadoInvalido = usuario.validarNumeroTelefonico();
        assertFalse(resultadoInvalido);
    }

    @Test
    void validarEdad() {
        Usuario usuario = new Usuario();

        // Prueba para una edad válida
        usuario.setEdad(25);
        boolean resultadoValido = usuario.validarEdad();
        assertTrue(resultadoValido);

        // Prueba para una edad inválida
        usuario.setEdad(150); // Edad fuera del rango esperado
        boolean resultadoInvalido = usuario.validarEdad();
        assertFalse(resultadoInvalido);
    }
}