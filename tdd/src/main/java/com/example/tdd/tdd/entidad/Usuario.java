package com.example.tdd.tdd.entidad;

public class Usuario {
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String rut;
    private String numeroTelefonico;
    private int edad;

    // Getters y Setters para los campos...

    public boolean validarNombre() {
        return nombre != null && !nombre.isEmpty();
    }

    public boolean validarApellidoPaterno() {
        return apellidoPaterno != null && !apellidoPaterno.isEmpty();
    }

    public boolean validarApellidoMaterno() {
        return apellidoMaterno != null && !apellidoMaterno.isEmpty();
    }

    public boolean validarRut() {
        // Implementación básica para validar un Rut
        return rut != null && rut.matches("\\d{1,2}\\.\\d{3}\\.\\d{3}-[\\dkK]");
    }

    public boolean validarNumeroTelefonico() {
        // Implementación básica para validar un número telefónico
        return numeroTelefonico != null && !numeroTelefonico.isEmpty();
    }

    public boolean validarEdad() {
        // Implementación básica para validar la edad en un rango específico
        return edad >= 18 && edad <= 120;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNumeroTelefonico() {
        return numeroTelefonico;
    }

    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}




