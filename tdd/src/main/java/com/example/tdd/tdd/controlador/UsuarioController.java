package com.example.tdd.tdd.controlador;

import com.example.tdd.tdd.entidad.Usuario;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @PostMapping("/usuarios")
    public ResponseEntity<String> validarUsuario(@RequestBody Usuario usuario) {
        //En terminos simples, valida el nombre y si es distinto a una validacion correcta dice que es incorrecto, en si no muestra a todos es correcto o incorrecto
        //solo muestra cuando es incorrecto , si hay 3 incorrectas, te dice las 3 que estan incorrectas
        StringBuilder mensaje = new StringBuilder();
        boolean esValido = true;

        if (!usuario.validarNombre()) {
            mensaje.append("El nombre es incorrecto. ");
            esValido = false;
        }

        if (!usuario.validarApellidoPaterno()) {
            mensaje.append("El apellido paterno es incorrecto. ");
            esValido = false;
        }

        if (!usuario.validarApellidoMaterno()) {
            mensaje.append("El apellido materno es incorrecto. ");
            esValido = false;
        }

        if (!usuario.validarRut()) {
            mensaje.append("El Rut es incorrecto. ");
            esValido = false;
        }

        if (!usuario.validarNumeroTelefonico()) {
            mensaje.append("El número telefónico es incorrecto. ");
            esValido = false;
        }

        if (!usuario.validarEdad()) {
            mensaje.append("La edad es incorrecta. ");
            esValido = false;
        }

        if (esValido) {
            return ResponseEntity.ok("El usuario es válido.");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mensaje.toString());
        }
    }
}
